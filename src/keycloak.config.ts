import { KeycloakConfig } from 'keycloak-angular';

export const keycloakConfig: KeycloakConfig = {
  clientId: 'bank-service',
  credentials: {
    secret: '9ddd382c-c9f8-47fe-a506-7f7f3dfd20ea'
  },
  realm: 'dev',
  url: 'http://192.168.99.100:8080/auth'
};
