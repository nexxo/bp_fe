import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../models/account';
import {GridTile} from '../models/banker';

@Component({
  selector: 'app-account-card',
  templateUrl: './account-card.component.html',
  styleUrls: ['./account-card.component.scss']
})
export class AccountCardComponent implements OnInit {

  constructor() { }

  @Input()
  account: Account;
  @Input()
  index: number;
  step = 0;

  gridTiles(): GridTile[] {
    return [
      {data: 'Zostatok', cols: 1, rows: 1, isHeader: true},
      {data: this.account.currentBalance.toFixed(2).toString(), cols: 2, rows: 1, isHeader: false},
      {data: 'Poznamka', cols: 1, rows: 1, isHeader: true},
      {data: this.account.note, cols: 2, rows: 1, isHeader: false},
      {data: 'Limit', cols: 1, rows: 1, isHeader: true},
      {data: this.account.overLimitValue.toFixed(2).toString(), cols: 2, rows: 1, isHeader: false},
    ];
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  ngOnInit() {
  }

}
