export interface VaultSupply {
  id: string;
  nominalValue: number;
  number: number;
  type: Type;
}

export enum Type {
  BANKNOTE,
  COIN
}

export interface VaultSupplyPick {
  vaultSupply: VaultSupply;
  amount: number;
}

export interface MoneyPickerResult {
  pickedMoney: VaultSupplyPick[];
  sum: number;
}

export interface Amount {
  currency: string;
  precision: number;
  value: number;
}
