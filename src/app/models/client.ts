import { Account } from './account';

export interface Client {
  id: string;
  forename: string;
  surname: string;
  birth: Date;
  identificationId: string;
  accounts: Account[];
}
