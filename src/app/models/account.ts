export interface Account {
  id: string;
  iban: string;
  currentBalance: number;
  note: string;
  name: string;
  overLimitValue: number;
  productDescription: string;
}
