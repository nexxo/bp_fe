import {Amount, VaultSupply, VaultSupplyPick} from './vaultSupply';
import {Moment} from 'moment';
import {Account} from './account';

export interface TransactionDetails {
  transactionDate: Moment;
  vaultSupplies: VaultSupplyPick[];
  value: number;
  note?: string;
}

export enum OrderCategory {
  DOMESTIC,
  FX
}

export enum State {
  CREATED,
  CANCELLED,
  CLOSED
}

export class Transaction {
  sourceAccount: Account;
  amount: Amount;
  clientId: string;
  createdBy: string;
  identificationId: string;
  modificationDate?: Date;
  note = '';
  orderCategory: OrderCategory;
  organizationalUnitID: string;
  state: State = State.CREATED;
  transferDate: Date;
  vault: VaultSupply[];


  constructor(
    sourceAccount: Account, amount: Amount, clientId: string, createdBy: string, identificationId: string,
    modificationDate: Date, note: string, orderCategory: OrderCategory, organizationalUnitID: string,
    state: State, transferDate: Date, vault: VaultSupply[]
  ) {
    this.sourceAccount = sourceAccount;
    this.amount = amount;
    this.clientId = clientId;
    this.createdBy = createdBy;
    this.identificationId = identificationId;
    this.modificationDate = modificationDate;
    this.note = note;
    this.orderCategory = orderCategory;
    this.organizationalUnitID = organizationalUnitID;
    this.state = state;
    this.transferDate = transferDate;
    this.vault = vault;
  }
}
