export interface GridTile {
  data?: string;
  rows: number;
  cols: number;
  isHeader: boolean;
}
