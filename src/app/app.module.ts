import {BrowserModule} from '@angular/platform-browser';
import {ApplicationRef, DoBootstrap, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavigationBarComponent} from './navigation-bar/navigation-bar.component';
import {MaterialModule} from './modules/material/material.module';
import {HttpClientModule} from '@angular/common/http';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {keycloakConfig} from '../keycloak.config';
import {AppRoutingModule} from './app-routing.module';
import {ProfileCardComponent} from './profile-card/profile-card.component';
import {BankerProfilePageComponent} from './page-components/banker-profile-page/banker-profile-page.component';
import {TransactionPageComponent} from './page-components/transaction-page/transaction-page.component';
import {SearchBarComponent} from './navigation-bar/search-bar/search-bar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ClientProfilePageComponent} from './page-components/client-profile-page/client-profile-page.component';
import {SpeedDialFabComponent} from './speed-dial-fab/speed-dial-fab.component';
import {AccountCardComponent} from './account-card/account-card.component';
import {HomePageComponent} from './page-components/home-page/home-page.component';
import {InformationDialogComponent} from './information-dialog/information-dialog.component';
import {TransactionSettingCardComponent} from './transaction-setting-card/transaction-setting-card.component';
import {TransactionDetailsCardComponent} from './transaction-details-card/transaction-details-card.component';
import {CustomProvidersModule} from './modules/custom-providers/custom-providers.module';
import {ServiceModule} from './modules/service/service.module';
import { MoneyPickerDialogComponent } from './money-picker-dialog/money-picker-dialog.component';


const keycloakService = new KeycloakService();

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    ProfileCardComponent,
    BankerProfilePageComponent,
    TransactionPageComponent,
    SearchBarComponent,
    ClientProfilePageComponent,
    SpeedDialFabComponent,
    AccountCardComponent,
    HomePageComponent,
    InformationDialogComponent,
    TransactionSettingCardComponent,
    TransactionDetailsCardComponent,
    MoneyPickerDialogComponent
  ],
  imports: [
    BrowserModule,
    KeycloakAngularModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CustomProvidersModule,
    ServiceModule
  ],
  providers: [
    {
      provide: KeycloakService,
      useValue: keycloakService
    }
  ],
  entryComponents: [AppComponent, InformationDialogComponent, MoneyPickerDialogComponent]
})
export class AppModule implements DoBootstrap {

  async ngDoBootstrap(appRef: ApplicationRef) {
    try {
      await keycloakService.init({
        config: keycloakConfig,
        initOptions: {
          onLoad: 'login-required',
          checkLoginIframe: false,
          responseMode: 'fragment',
          flow: 'implicit',
          promiseType: 'legacy',
        },
        enableBearerInterceptor: true,
        bearerExcludedUrls: ['/assets', '/login']
      });
      appRef.bootstrap(AppComponent);
    } catch (error) {
      console.error('Keycloak init failed', error);
    }
  }
}
