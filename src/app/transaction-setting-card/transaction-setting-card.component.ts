import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Account} from '../models/account';
import {FormControl, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-transaction-setting-card',
  templateUrl: './transaction-setting-card.component.html',
  styleUrls: ['./transaction-setting-card.component.scss']
})
export class TransactionSettingCardComponent implements OnInit, OnDestroy {

  @Input()
  private accounts: Account[];

  private ibanFormControl: FormControl;
  private changeSub$: Subscription;

  @Output()
  accountChange = new EventEmitter<Account>();

  constructor() {
  }

  ngOnInit() {
    this.ibanFormControl = new FormControl('', [Validators.required]);
    this.changeSub$ = this.ibanFormControl.valueChanges
        .subscribe((account: Account) => {
          this.accountChange.emit(account);
        });
  }

  ngOnDestroy(): void {
    this.changeSub$.unsubscribe();
  }

}
