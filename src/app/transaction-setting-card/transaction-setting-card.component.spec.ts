import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionSettingCardComponent } from './transaction-setting-card.component';

describe('TransactionSettingCardComponent', () => {
  let component: TransactionSettingCardComponent;
  let fixture: ComponentFixture<TransactionSettingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionSettingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSettingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
