import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyPickerDialogComponent } from './money-picker-dialog.component';

describe('MoneyPickerDialogComponent', () => {
  let component: MoneyPickerDialogComponent;
  let fixture: ComponentFixture<MoneyPickerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyPickerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyPickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
