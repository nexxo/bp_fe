import {Component, OnDestroy, OnInit} from '@angular/core';
import {VaultSupplyService} from '../services/vault-supply/vault-supply.service';
import {VaultSupply, VaultSupplyPick} from '../models/vaultSupply';
import {MatDialogRef} from '@angular/material';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-money-picker-dialog',
  templateUrl: './money-picker-dialog.component.html',
  styleUrls: ['./money-picker-dialog.component.scss']
})
export class MoneyPickerDialogComponent implements OnInit, OnDestroy {

  private sum = 0;
  private vaultSupplies: VaultSupply[] = [];
  private vaultSuppliesChangeSub$: Subscription;
  private formGroup: FormGroup;

  private readonly displayedColumns = [
    'nominalValue',
    'selectedNumber',
    'number'
  ];

  constructor(
    private readonly vaultSupplyService: VaultSupplyService,
    public dialogRef: MatDialogRef<MoneyPickerDialogComponent>,
  ) {
  }

  ngOnInit() {
    this.vaultSupplyService.getVaultSupplies()
      .subscribe((vaultSupplies: VaultSupply[]) => {
        this.vaultSupplies = vaultSupplies;
        this.initFormGroup(vaultSupplies);
        this.vaultSuppliesChangeSub$ = this.formGroup.valueChanges.subscribe(() => {
          this.recalculateSum();
        });
      });

    this.formGroup = new FormGroup({});
  }

  ngOnDestroy(): void {
    if (this.vaultSuppliesChangeSub$) {
      this.vaultSuppliesChangeSub$.unsubscribe();
    }
  }

  private initFormGroup(vaultSupplies: VaultSupply[]): void {
    vaultSupplies.forEach((vaultSupply: VaultSupply) => {
      const formControl = new FormControl(
        0,
        [Validators.min(0), Validators.max(vaultSupply.number), this.validateNumericInput()]
      );

      this.formGroup.addControl(this.getId(vaultSupply.nominalValue), formControl);
    });
  }

  cancel(): void {
    this.dialogRef.close(null);
  }

  approve(): void {
    this.dialogRef.close({pickedMoney: this.collectMoney(), sum: this.sum});
  }

  private collectMoney(): VaultSupplyPick[] {
    const pickedMoney: VaultSupplyPick[] = [];

    this.vaultSupplies.forEach((vaultSupply: VaultSupply) => {
        pickedMoney.push({
          vaultSupply,
          amount: this.getFormControl(vaultSupply.nominalValue).value
        });
      });

    return pickedMoney;
  }

  private getId = (nominalValue: number): string => nominalValue.toString().replace('.', '');

  private getFormControl(nominalValue: number): FormControl {
    return this.formGroup.get(this.getId(nominalValue)) as FormControl;
  }

  recalculateSum(): void {
    this.sum = 0;
    this.vaultSupplies.forEach((vaultSupply: VaultSupply) => {
      const formControl = this.getFormControl(vaultSupply.nominalValue);
      if (formControl.valid) {
        this.sum += formControl.value * vaultSupply.nominalValue;
      }
    });
  }

  validateNumericInput(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return (typeof control.value === 'number' && /^\d+$/.test(control.value.toString())) ? null : {invalid: control.value};
    };
  }
}
