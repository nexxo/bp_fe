import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Moment} from 'moment';
import {MatDialog} from '@angular/material';
import {MoneyPickerDialogComponent} from '../money-picker-dialog/money-picker-dialog.component';
import {MoneyPickerResult, VaultSupplyPick} from '../models/vaultSupply';
import {TransactionDetails} from '../models/transaction';
import {pipe, Subscription} from 'rxjs';
import {filter, map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-transaction-details-card',
  templateUrl: './transaction-details-card.component.html',
  styleUrls: ['./transaction-details-card.component.scss']
})
export class TransactionDetailsCardComponent implements OnInit, OnDestroy {

  @Output()
  private changePick = new EventEmitter<TransactionDetails>();
  @Input()
  private title: string;
  private changeSub$: Subscription;
  detailsFormGroup: FormGroup;
  private amount = 0;
  private moneyPickerResult: MoneyPickerResult | null = null;

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog) {
  }

  private minimumDate(): Moment {
    const currentDate = moment();
    currentDate.locale('sk');
    // add 3 days till next possible transaction
    currentDate.add(4, 'days');

    if (currentDate.isoWeekday() === 6) { // saturday
      currentDate.add(2, 'days');
    }
    if (currentDate.isoWeekday() === 7) { // sunday
      currentDate.add(1, 'days');
    }

    return currentDate;
  }

  private dateFilter(date: Moment): boolean {
    return date.isoWeekday() !== 6 && date.isoWeekday() !== 7;
  }

  ngOnInit(): void {
    this.detailsFormGroup = this.formBuilder.group({
      amount: [{value: 0.0, disabled: true}, Validators.required, Validators.min(0.1)],
      transactionDate: [this.minimumDate(), Validators.required],
      note: ['', Validators.maxLength(500)]
    });
    this.changeSub$ = this.detailsFormGroup.valueChanges.subscribe(() => this.emitDetailsIfValid());
  }

  ngOnDestroy(): void {
    this.changeSub$.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MoneyPickerDialogComponent, {minWidth: '300px'});

    dialogRef
      .afterClosed()
      .subscribe((moneyPickerResult: MoneyPickerResult) => {
        if (moneyPickerResult) {
          moneyPickerResult = this.filterValidBankPick(moneyPickerResult);
          this.moneyPickerResult = moneyPickerResult;
          this.amount = moneyPickerResult.sum;
        }
      });
  }

  private filterValidBankPick(moneyPickerResult: MoneyPickerResult): MoneyPickerResult {
    moneyPickerResult.pickedMoney = moneyPickerResult
      .pickedMoney
      .filter((vaultSupplyPick: VaultSupplyPick) => vaultSupplyPick.amount !== 0);

    return moneyPickerResult;
  }

  private emitDetailsIfValid(): void {
    if (this.detailsFormGroup.valid && this.moneyPickerResult) {
      this.changePick.emit({
        note: this.detailsFormGroup.get('note').value,
        transactionDate: this.detailsFormGroup.get('transactionDate').value,
        vaultSupplies: this.moneyPickerResult.pickedMoney,
        value: this.moneyPickerResult.sum
      });
    }
  }
}
