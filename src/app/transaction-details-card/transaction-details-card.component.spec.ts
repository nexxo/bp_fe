import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionDetailsCardComponent } from './transaction-details-card.component';

describe('TransactionDetailsCardComponent', () => {
  let component: TransactionDetailsCardComponent;
  let fixture: ComponentFixture<TransactionDetailsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionDetailsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
