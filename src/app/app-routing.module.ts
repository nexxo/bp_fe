import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CanAuthenticationGuardService } from './services/auth/can-authentication-guard.service';
import {BankerProfilePageComponent} from './page-components/banker-profile-page/banker-profile-page.component';
import {TransactionPageComponent} from './page-components/transaction-page/transaction-page.component';
import {ClientProfilePageComponent} from './page-components/client-profile-page/client-profile-page.component';
import {HomePageComponent} from './page-components/home-page/home-page.component';


const routes: Route[] = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'banker-profile',
    component: BankerProfilePageComponent,
    canActivate: [CanAuthenticationGuardService]
  },
  {
    path: 'transactions', // todo finish implementation
    component: TransactionPageComponent,
    canActivate: [CanAuthenticationGuardService]
  },
  {
    path: 'client-profile/:identificationId',
    component: ClientProfilePageComponent,
    canActivate: [CanAuthenticationGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
