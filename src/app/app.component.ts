import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { AuthorizationService } from './services/auth/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'bp-fe';

  constructor(
    private keycloakService: KeycloakService,
    private readonly authorizationService: AuthorizationService
  ) { }

  ngOnInit(): void {
    // this.keycloakService
    //   .getToken()
    //   .then((token: string) => this.authorizationService.setToken(token));

  }
}

export const HOST = 'http://localhost:8081';
export const VERSION = 1;
export const DEFAULT_URL = `${HOST}/v${VERSION}`;
