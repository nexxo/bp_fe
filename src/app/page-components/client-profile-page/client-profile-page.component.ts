import {Component, OnDestroy, OnInit} from '@angular/core';
import {GridTile} from '../../models/banker';
import {ClientService} from '../../services/client/client.service';
import {Client} from '../../models/client';
import {ActivatedRoute, Params} from '@angular/router';
import {Subscription} from 'rxjs';
import {ClientContextService} from '../../services/client/client-context.service';
import {SnackBarService} from '../../services/material/snack-bar.service';

@Component({
  selector: 'app-client-profile-page',
  templateUrl: './client-profile-page.component.html',
  styleUrls: ['./client-profile-page.component.scss']
})
export class ClientProfilePageComponent implements OnInit, OnDestroy {

  constructor(
    private readonly clientService: ClientService,
    private readonly route: ActivatedRoute,
    private readonly clientContextService: ClientContextService,
    private readonly snackBarService: SnackBarService
  ) {
  }

  private cardTitle = 'Profil klienta';
  private paramSub$: Subscription;
  private client: Client;
  private clientIdentificationId: string;

  static formatDate(dateString: string) {
    const date = new Date(/\d{4}-\d{2}-\d{2}/.exec(dateString)[0]);

    return date.toLocaleDateString('sk-SK');
  }

  ngOnInit() {
    this.paramSub$ = this.route.params.subscribe((params: Params) => {
      this.clientIdentificationId = params.identificationId;
    });

    this.clientService
      .getClientByIdentificationId(this.clientIdentificationId)
      .subscribe((client: Client) => this.client = client);
  }

  ngOnDestroy(): void {
    this.paramSub$.unsubscribe();
  }

  private getClientContext(): Client {
    return this.clientContextService.getClientContext();
  }

  private selectClient(client: Client): void {
    this.clientContextService.setClientContext(client);
    const message = `Pracujete s klientom:  ${client.forename} ${client.surname} (${client.identificationId})`;
    this.snackBarService.openSnackBar(message, null, 'blue-snackbar');
  }

  private gridTiles(): GridTile[] {
    return [
      {data: 'Meno', cols: 1, rows: 1, isHeader: true},
      {data: this.client.forename, cols: 2, rows: 1, isHeader: false},
      {data: 'Priezvisko', cols: 1, rows: 1, isHeader: true},
      {data: this.client.surname, cols: 2, rows: 1, isHeader: false},
      {data: 'Narodenie', cols: 1, rows: 1, isHeader: true},
      {data: ClientProfilePageComponent.formatDate(this.client.birth.toString()), cols: 2, rows: 1, isHeader: false},
      {data: 'Identifikacia', cols: 1, rows: 1, isHeader: true},
      {data: this.client.identificationId, cols: 2, rows: 1, isHeader: false}
    ];
  }
}
