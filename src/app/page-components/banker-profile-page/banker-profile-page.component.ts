import {Component, OnDestroy, OnInit} from '@angular/core';
import {from, Subscription} from 'rxjs';
import { KeycloakProfile } from 'keycloak-js';
import { AuthorizationService } from '../../services/auth/authorization.service';
import { GridTile } from '../../models/banker';
import {ClientContextService} from '../../services/client/client-context.service';
import {Client} from '../../models/client';

@Component({
  selector: 'app-banker-profile-page',
  templateUrl: './banker-profile-page.component.html',
  styleUrls: ['./banker-profile-page.component.scss']
})
export class BankerProfilePageComponent implements OnInit, OnDestroy {

  private profile: KeycloakProfile;
  private cardTitle = 'Profil pracovnika';
  private loginSub$: Subscription;

  constructor(
    private readonly authorizationService: AuthorizationService,
    private readonly clientContextService: ClientContextService) {}

  ngOnInit(): void {
    const response = from(this.authorizationService.getLoggedUser());

    this.loginSub$ = response.subscribe((profile: KeycloakProfile) => this.profile = profile);
  }

  ngOnDestroy(): void {
    this.loginSub$.unsubscribe();
  }

  gridTiles(): GridTile[] {
    return [
      {data: 'Meno', cols: 1, rows: 1, isHeader: true},
      {data: this.profile.firstName, cols: 2, rows: 1, isHeader: false},
      {data: 'Priezvisko', cols: 1, rows: 1, isHeader: true},
      {data: this.profile.lastName, cols: 2, rows: 1, isHeader: false},
      {data: 'Mail', cols: 1, rows: 1, isHeader: true},
      {data: this.profile.email, cols: 2, rows: 1, isHeader: false},
      {data: 'Id', cols: 1, rows: 1, isHeader: true},
      {data: '123on123m', cols: 2, rows: 1, isHeader: false},
    ];
  }

  private getClientContext(): Client {
    return this.clientContextService.getClientContext();
  }
}
