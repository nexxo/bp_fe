import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankerProfilePageComponent } from './banker-profile-page.component';

describe('BankerProfilePageComponent', () => {
  let component: BankerProfilePageComponent;
  let fixture: ComponentFixture<BankerProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankerProfilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankerProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
