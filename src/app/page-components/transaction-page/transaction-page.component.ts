import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ClientContextService} from '../../services/client/client-context.service';
import {InformationDialogComponent} from '../../information-dialog/information-dialog.component';
import {Client} from '../../models/client';
import {GridTile} from '../../models/banker';
import {ClientProfilePageComponent} from '../client-profile-page/client-profile-page.component';
import {Account} from '../../models/account';
import {Subscription} from 'rxjs';
import {TransactionService} from '../../services/transaction/transaction.service';
import {OrderCategory, Transaction, TransactionDetails} from '../../models/transaction';
import {VaultSupply, VaultSupplyPick} from '../../models/vaultSupply';
import {SnackBarService} from '../../services/material/snack-bar.service';

@Component({
  selector: 'app-transaction-page',
  templateUrl: './transaction-page.component.html',
  styleUrls: ['./transaction-page.component.scss']
})
export class TransactionPageComponent implements OnInit, OnDestroy {

  selectedAccount?: Account = null;
  client: Client;
  private clientSub$: Subscription;
  private transactionDetails?: TransactionDetails;

  constructor(
    public dialog: MatDialog,
    private readonly clientContextService: ClientContextService,
    private readonly transactionService: TransactionService,
    private readonly snackBarService: SnackBarService
  ) {
  }

  ngOnInit() {
    this.clientSub$ = this.clientContextService.clientContextSubject$
      .subscribe((client: Client) => this.client = client);

    if (!this.clientContextService.getClientContext()) {
      this.openDialog();
    }
  }

  ngOnDestroy(): void {
    this.clientSub$.unsubscribe();
  }

  openDialog(): void {
    this.dialog.open(InformationDialogComponent);
  }

  gridTiles(): GridTile[] {
    return [
      {data: 'Meno', cols: 1, rows: 1, isHeader: true},
      {data: this.client.forename, cols: 2, rows: 1, isHeader: false},
      {data: 'Priezvisko', cols: 1, rows: 1, isHeader: true},
      {data: this.client.surname, cols: 2, rows: 1, isHeader: false},
      {data: 'Narodenie', cols: 1, rows: 1, isHeader: true},
      {data: ClientProfilePageComponent.formatDate(this.client.birth.toString()), cols: 2, rows: 1, isHeader: false},
      {data: 'Identifikacia', cols: 1, rows: 1, isHeader: true},
      {data: this.client.identificationId, cols: 2, rows: 1, isHeader: false}
    ];
  }

  setAccount(account: Account): void {
    this.selectedAccount = account;
  }

  setTransactionDetails(transactionDetails: TransactionDetails): void {
    this.transactionDetails = transactionDetails;
  }

  private createTransaction(): Transaction | null {
    if (this.transactionDetails && this.client && this.selectedAccount) {
      return this.transactionService.createTransaction(
        this.selectedAccount, 'EUR', 2, this.transactionDetails.value, this.client.id, 'me',
        this.client.identificationId, null, this.transactionDetails.note, OrderCategory.DOMESTIC, '123',
        this.transactionDetails.transactionDate, this.getPickedVaultSupplies()
      );
    }
  }

  private getPickedVaultSupplies(): VaultSupply[] {
    return [...this.transactionDetails.vaultSupplies]
      .map((vaultSupplyPick: VaultSupplyPick) => {
        vaultSupplyPick.vaultSupply.number = vaultSupplyPick.amount;
        return vaultSupplyPick.vaultSupply;
      });
  }

  commitTransaction(): void {
    if (this.transactionDetails && this.client && this.selectedAccount) {
      const transaction = this.createTransaction();
      this.transactionService.setTargetTransaction(transaction);
      this.transactionService
        .commitTransaction()
        .subscribe(
          () => this.snackBarService.openSnackBar('Transakcia prebehla uspesne', null, 'blue-snackbar'),
          () => this.snackBarService.openSnackBar('Transakcia neprebehla uspesne', null, 'red-snackbar')
        );
      this.clearTransactionDetails();
    } else {
      this.snackBarService.openSnackBar('Nedostatocne vyplneny formular', null, 'red-snackbar');
    }
  }

  private clearTransactionDetails(): void {
    this.transactionDetails = null;
  }
}
