import { Component, OnInit } from '@angular/core';
import {WindowService} from '../../services/window/window.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  private resizeSub$: Subscription;
  private MOBILE_FIT = 500;
  public windowWidth: number;
  public breakpointReached: boolean;

  constructor(private readonly windowService: WindowService) { }

  ngOnInit() {
    this.resizeSub$ = this.windowService.resizeSubject$
      .subscribe((width: number) => {
        this.windowWidth = width;
        this.breakpointReached = this.windowWidth < this.MOBILE_FIT;
      });
  }

}
