import {Component, Input, OnInit} from '@angular/core';
import {speedDialFabAnimations} from './speed-dial-fab-animations';
import {SupervisorAction} from './speed-dial-actions';
import {Router} from '@angular/router';
import {ClientContextService} from '../services/client/client-context.service';


export interface FabButton {
  icon: string;
  tooltip: string;
  action: SupervisorAction;
}


@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.scss'],
  animations: [speedDialFabAnimations]
})
export class SpeedDialFabComponent implements OnInit {

  constructor(private readonly router: Router, private readonly clientContextService: ClientContextService) {
  }

  @Input() reverseColumnDirection = false;

  private buttons = [];
  private fabTogglerState = 'inactive';
  private readonly fabButtons = [
    {
      icon: 'person_add_disabled',
      tooltip: 'prestat pracovat s klientom',
      action: SupervisorAction.REMOVE_CLIENT
    },
    {
      icon: 'account_box',
      tooltip: 'prejst na profil klienta',
      action: SupervisorAction.VIEW_CLIENT_PROFILE
    },
    {
      icon: 'local_atm',
      tooltip: 'prejst na transakcie',
      action: SupervisorAction.VIEW_TRANSACTIONS
    },
  ];

  ngOnInit() {
  }

  private showItems(): void {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  private hideItems(): void {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  private fabAction(func: () => void) {
    this.hideItems();
    func();
  }

  private redirect(path: string, param: string | null): Promise<boolean> {
    return this.router.navigate(param === null ? [path] : [path, param]);
  }

  public onToggleFab(): void {
    this.buttons.length ? this.hideItems() : this.showItems();
  }

  public onClickFab(btn: FabButton) {
    switch (btn.action) {
      case SupervisorAction.VIEW_CLIENT_PROFILE:
        this.fabAction(() => {
          this.redirect('/client-profile', this.clientContextService.getClientContext().identificationId).then();
        });
        break;
      case SupervisorAction.REMOVE_CLIENT:
        this.fabAction(() => this.clientContextService.clearClientContext());
        break;
      case SupervisorAction.VIEW_TRANSACTIONS:
        this.fabAction(() => {
          this.redirect('/transactions', null).then();
        });
        break;
    }
  }
}
