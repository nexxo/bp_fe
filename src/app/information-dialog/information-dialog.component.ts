import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './information-dialog.component.html',
  styleUrls: ['./information-dialog.component.scss']
})
export class InformationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<InformationDialogComponent>) { }

  ngOnInit() {
  }

  private onOkClick(): void {
    this.dialogRef.close();
  }
}
