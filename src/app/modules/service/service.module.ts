import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthorizationService} from '../../services/auth/authorization.service';
import {CanAuthenticationGuardService} from '../../services/auth/can-authentication-guard.service';
import {WindowService} from '../../services/window/window.service';
import {ClientService} from '../../services/client/client.service';
import {ClientContextService} from '../../services/client/client-context.service';
import {SnackBarService} from '../../services/material/snack-bar.service';
import {VaultSupplyService} from '../../services/vault-supply/vault-supply.service';
import {TransactionService} from '../../services/transaction/transaction.service';
import {TransactionWebService} from '../../services/transaction/transaction-web.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AuthorizationService,
    CanAuthenticationGuardService,
    WindowService,
    ClientService,
    ClientContextService,
    SnackBarService,
    VaultSupplyService,
    TransactionService,
    TransactionWebService
  ]
})
export class ServiceModule { }
