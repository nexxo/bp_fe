import { TestBed } from '@angular/core/testing';

import { CanAuthenticationGuardService } from './can-authentication-guard.service';

describe('CanAuthenticationGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanAuthenticationGuardService = TestBed.get(CanAuthenticationGuardService);
    expect(service).toBeTruthy();
  });
});
