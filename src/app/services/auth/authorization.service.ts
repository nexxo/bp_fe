import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';

@Injectable()
export class AuthorizationService {

  constructor(private readonly keycloakService: KeycloakService) {}

  getLoggedUser(): Promise<KeycloakProfile> {
    return this.keycloakService.loadUserProfile();
  }

  logout(): Promise<void> {
    const redirectLogoutUri = 'http://localhost:4200';
    return this.keycloakService.logout(redirectLogoutUri);
  }

  isUserLoggedIn(): Observable<boolean> {
    return fromPromise(this.keycloakService.isLoggedIn());
  }
}
