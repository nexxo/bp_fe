import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, fromEvent, Subscription } from 'rxjs';

@Injectable()
export class WindowService implements OnDestroy {
  public resizeSubject$ = new BehaviorSubject<number>(window.innerWidth);
  private subscription$: Subscription;

  constructor() {
    this.subscription$ = fromEvent(window, 'resize')
      // @ts-ignore
      .subscribe((e: Event) => this.updateSubject(e.target.innerWidth));
  }

  ngOnDestroy(): void {
    this.subscription$.unsubscribe();
  }

  updateSubject(width: number) {
    this.resizeSubject$.next(width);
  }
}
