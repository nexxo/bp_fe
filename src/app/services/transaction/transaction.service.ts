import {Injectable} from '@angular/core';
import {TransactionWebService} from './transaction-web.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {OrderCategory, State, Transaction} from '../../models/transaction';
import {Account} from '../../models/account';
import {VaultSupply} from '../../models/vaultSupply';
import {Moment} from 'moment';

@Injectable()
export class TransactionService {

  transactionSubject$ = new BehaviorSubject<Transaction>(null);

  constructor(private readonly transactionWebService: TransactionWebService) {
  }

  setTargetTransaction(transaction: Transaction): void {
    this.transactionSubject$.next(transaction);
  }

  commitTransaction(): Observable<Transaction> {
    const transaction = this.transactionSubject$.getValue();
    if (transaction) {
      return this.transactionWebService.postTransaction(transaction);
    }
    this.transactionSubject$.next(null);
    return of();
  }

  createTransaction(
    sourceAccount: Account, currency: string, precision: number, value: number, clientId: string, createdBy: string,
    identificationId: string, modificationDate: Moment, note: string, orderCategory: OrderCategory, organizationalUnitID: string,
    transferDate: Moment, vault: VaultSupply[]
  ): Transaction | null {

    return new Transaction(
      sourceAccount, {currency, precision, value}, clientId, createdBy, identificationId,
      modificationDate ? modificationDate.toDate() : null, note, orderCategory, organizationalUnitID, State.CREATED,
      transferDate.toDate(), vault
    );
  }
}
