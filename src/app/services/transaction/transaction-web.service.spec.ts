import { TestBed } from '@angular/core/testing';

import { TransactionWebService } from './transaction-web.service';

describe('TransactionWebService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransactionWebService = TestBed.get(TransactionWebService);
    expect(service).toBeTruthy();
  });
});
