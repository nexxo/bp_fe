import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Transaction} from '../../models/transaction';
import {Observable} from 'rxjs';
import {DEFAULT_URL} from '../../app.component';

@Injectable()
export class TransactionWebService {

  private URL_POSTFIX = '/transaction';

  constructor(private readonly restClient: HttpClient) { }

  postTransaction(transaction: Transaction): Observable<Transaction> {
    return this.restClient.post<Transaction>(DEFAULT_URL + this.URL_POSTFIX, transaction);
  }
}
