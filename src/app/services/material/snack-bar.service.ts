import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {
  }

  openSnackBar(message: string, action: string, className: string): void {
    this.snackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'left',
      panelClass: [className]
    });
  }
}
