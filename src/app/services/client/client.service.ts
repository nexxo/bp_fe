import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../../models/client';
import { DEFAULT_URL } from '../../app.component';

@Injectable()
export class ClientService {

  private URL_POSTFIX = '/client';

  constructor(
    private readonly restClient: HttpClient
  ) { }

  getClientsByIdentificationIdStartsWith(identificationId: string): Observable<Client[]> {
    return this.restClient.get<Client[]>(`${DEFAULT_URL + this.URL_POSTFIX}/${identificationId}`);
  }

  getClientByIdentificationId(identificationId: string): Observable<Client> {
    return this.restClient.get<Client>(`${DEFAULT_URL + this.URL_POSTFIX}?identificationId=${identificationId}`);
  }
}
