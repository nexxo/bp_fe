import {Injectable} from '@angular/core';
import {Client} from '../../models/client';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class ClientContextService {

  private clientKey = 'clientContext';
  clientContextSubject$ = new BehaviorSubject<Client>(this.getItemValueFromSession(this.clientKey));

  constructor() {
  }

  setClientContext(client: Client): void {
    if (this.clientContextSubject$.getValue() !== client) {
      sessionStorage.setItem(this.clientKey, JSON.stringify(client));
      this.clientContextSubject$.next(client);
    }
  }

  clearClientContext(): void {
    sessionStorage.removeItem(this.clientKey);
    this.clientContextSubject$.next(null);
  }

  getClientContext(): Client {
    const currentClientContext = this.clientContextSubject$.getValue();
    return (currentClientContext ? currentClientContext : this.getItemValueFromSession(this.clientKey));
  }

  private getItemValueFromSession(key: string): Client | null {
    return JSON.parse(sessionStorage.getItem(key));
  }
}
