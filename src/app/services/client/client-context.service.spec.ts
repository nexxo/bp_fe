import { TestBed } from '@angular/core/testing';

import { ClientContextService } from './client-context.service';

describe('ClientContextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientContextService = TestBed.get(ClientContextService);
    expect(service).toBeTruthy();
  });
});
