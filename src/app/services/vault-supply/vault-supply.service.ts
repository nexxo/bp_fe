import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {VaultSupply} from '../../models/vaultSupply';
import {DEFAULT_URL} from '../../app.component';

@Injectable()
export class VaultSupplyService {

  private URL_POSTFIX = '/vault-supplies';

  constructor(private readonly restClient: HttpClient) {
  }

  getVaultSupplies(): Observable<VaultSupply[]> {
    return this.restClient.get<VaultSupply[]>(DEFAULT_URL + this.URL_POSTFIX);
  }
}
