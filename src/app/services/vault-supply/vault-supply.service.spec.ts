import { TestBed } from '@angular/core/testing';

import { VaultSupplyService } from './vault-supply.service';

describe('VaultSupplyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VaultSupplyService = TestBed.get(VaultSupplyService);
    expect(service).toBeTruthy();
  });
});
