import { Component, Input, OnInit } from '@angular/core';
import { GridTile } from '../models/banker';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {

  @Input()
  private tiles: GridTile[];

  @Input()
  private title: string;

  constructor() { }

  ngOnInit() {

  }
}
