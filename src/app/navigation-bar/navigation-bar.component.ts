import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {WindowService} from '../services/window/window.service';
import {Subscription} from 'rxjs';
import {AuthorizationService} from '../services/auth/authorization.service';
import {ClientContextService} from '../services/client/client-context.service';
import {Client} from '../models/client';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {ClientService} from '../services/client/client.service';
import {TransactionService} from '../services/transaction/transaction.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit, OnDestroy {

  private resizeSub$: Subscription;
  private clientSub$: Subscription;
  private isUserLoggedSub$: Subscription;
  public windowWidth: number;
  public breakpointReached: boolean;
  private MOBILE_FIT = 500;
  private expandMobileSearch = false;
  private isUserLoggedIn = false;
  private client: Client;

  @ViewChild('searchBar', {static: false})
  private searchBarComponent: SearchBarComponent;

  constructor(
    private readonly router: Router,
    private readonly windowService: WindowService,
    private readonly authorizationService: AuthorizationService,
    private readonly clientContextService: ClientContextService,
    private readonly clientService: ClientService,
    private readonly transactionService: TransactionService
  ) {
  }

  ngOnInit(): void {
    this.resizeSub$ = this.windowService.resizeSubject$
      .subscribe((width: number) => {
        this.windowWidth = width;
        this.breakpointReached = this.windowWidth < this.MOBILE_FIT;
      });

    this.isUserLoggedSub$ = this.authorizationService
      .isUserLoggedIn()
      .subscribe((isUserLoggedId: boolean) => this.isUserLoggedIn = isUserLoggedId);

    this.clientSub$ = this.clientContextService.clientContextSubject$
      .subscribe((client: Client) => {
        this.client = client;
      });
  }

  toggleSearchBarExpansion() {
    this.expandMobileSearch = !this.expandMobileSearch;
  }

  ngOnDestroy(): void {
    this.resizeSub$.unsubscribe();
    this.isUserLoggedSub$.unsubscribe();
    this.clientSub$.unsubscribe();
  }

  private getSearchTerm(): string {
    return this.searchBarComponent.getSearchTerm();
  }

  private redirect(path: string, param: string | null): Promise<boolean> {
    return this.router.navigate(param === null ? [path] : [path, param]);
  }

  clientChipHandler(): void {
    const client = this.getClient();

    if (client) {
      this.redirect('/client-profile', client.identificationId).then();
    } else {
      this.clientService
        .getClientByIdentificationId(this.getSearchTerm().trim().toUpperCase())
        .subscribe(
          (data: Client) => {
            data ? this.clientContextService.setClientContext(data) : this.searchBarComponent.editSearchTextOnClick();
          }
        );
    }
  }

  getClient(): Client {
    return this.clientContextService.getClientContext();
  }

  deselectClient(): void {
    this.clientContextService.clearClientContext();
    this.transactionService.transactionSubject$.next(null);
  }

  logout(): Promise<void> {
    return this.authorizationService.logout();
  }
}
