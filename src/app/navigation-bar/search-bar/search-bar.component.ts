import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {WindowService} from '../../services/window/window.service';
import {Observable, of, Subscription} from 'rxjs';
import {ClientService} from '../../services/client/client.service';
import {debounceTime, distinctUntilChanged, finalize, switchMap, tap} from 'rxjs/operators';
import {Client} from '../../models/client';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {ClientContextService} from '../../services/client/client-context.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnDestroy {

  @ViewChild('searchInput', {static: true})
  private searchInput: ElementRef;

  private resizeSub$: Subscription;
  private clientFormSub$: Subscription;
  public windowWidth: number;
  public breakpointReached: boolean;
  private MOBILE_FIT = 500;
  private isLoading = false;
  private clients: Client[];
  private clientForm: FormGroup;

  constructor(
    private readonly windowService: WindowService,
    private readonly clientService: ClientService,
    private readonly clientContextService: ClientContextService,
    private readonly router: Router,
    private fb: FormBuilder
  ) {
    this.windowWidth = windowService.resizeSubject$.getValue();
  }

  ngOnInit() {
    this.monitorResize();

    this.clientForm = this.fb.group({
      searchTerm: ''
    });

    this.clientFormSub$ = this.clientForm
      .get('searchTerm')
      .valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => this.isLoading = true),
        switchMap((searchTerm: string) =>
          this.searchClientsByIdentificationIdStartsWith(searchTerm)
            .pipe(
              finalize(() => this.isLoading = false)
            ))
      )
      .subscribe((clients: Client[]) => this.clients = clients);
  }

  ngOnDestroy(): void {
    this.resizeSub$.unsubscribe();
    this.clientFormSub$.unsubscribe();
  }

  private redirect(path: string, param: string): Promise<boolean> {
    return this.router.navigate([path, param]);
  }

  private clear(): void {
    this.clientForm.get('searchTerm').setValue('');
  }

  getSearchTerm(): string {
    return this.clientForm.get('searchTerm').value;
  }

  private searchClientsByIdentificationIdStartsWith(id: string): Observable<Client[]> {
    if (id === '') {
      return of([]);
    }
    return this.clientService.getClientsByIdentificationIdStartsWith(id.trim().toUpperCase());
  }

  private monitorResize(): void {
    this.resizeSub$ = this.windowService.resizeSubject$
      .subscribe((width: number) => {
        this.windowWidth = width;
        this.breakpointReached = this.windowWidth < this.MOBILE_FIT;
      });
  }

  displayIdentificationId(identificationId: string): string {
    if (this.clientForm) {
      this.clientForm.get('searchTerm').setValue(identificationId);
    }
    return identificationId;
  }

  editSearchText(): void {
    this.searchInput.nativeElement.focus();
  }

  editSearchTextOnClick(): void {
    if (this.getSearchTerm() === '') {
      this.searchInput.nativeElement.focus();
    }
  }

  selectClient(): void {
    this.clientService
      .getClientByIdentificationId(this.getSearchTerm().toUpperCase().trim())
      .subscribe((client: Client) => {
        if (client) {
          this.clientContextService.setClientContext(client);
        }
      });
  }
}
